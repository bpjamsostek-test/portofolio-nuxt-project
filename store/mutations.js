export default {
  SET_IS_LOADING(state, value) {
    state.isLoading = value
  },
  SET_STEP_ACTIVE(state, value) {
    state.stepActive = value
  },
  SET_PROFILE_CODE(state, value) {
    state.profileCode = value
  }
}
