import Axios from 'axios'

Axios.defaults.baseURL = 'http://portofolio-api.test'
Axios.defaults.headers.post['Content-Type'] = 'application/json'
Axios.defaults.headers.post['Accept'] = 'application/json'

const api = {
  profile: {
    store: (data) => {
      return Axios.post('/api/profile', data)
    },
    show: (id) => {
      return Axios.get('/api/profile/{id}'.replace('{id}', id))
    },
    update: (id, data) => {
      return Axios.put('/api/profile/{id}'.replace('{id}', id), data)
    }
  },
  photo: {
    show: (id) => {
      return Axios.get('/api/photo/{id}'.replace('{id}', id))
    },
    update: (id, data) => {
      return Axios.put('/api/photo/{id}'.replace('{id}', id), data)
    },
    destroy: (id) => {
      return Axios.get('/api/photo/{id}'.replace('{id}', id))
    },
  }
}
export default api
